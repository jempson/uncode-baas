package cn.uncode.baas.server.service;

import cn.uncode.dal.descriptor.QueryResult;

import java.util.List;
import java.util.Map;

public interface IGenericService {
    
	QueryResult selectByCriteria(List<String> fields, Map<String, Object> params, String table, int seconds, String database);
	
	QueryResult selectByIds(List<String> fields, Map<String, Object[]> params, String table, int seconds, String database);
    
    QueryResult selectByPrimaryKey(List<String> fields, Object id, String table, int seconds, String database);
    
    int countByCriteria(Map<String, Object> params, String table, int seconds, String database);
    
    int deleteByPrimaryKey(Object id, String table, String database);
    
    int deleteByCriteria(Map<String, Object> params, String table, String database);
    
    int updateByPrimaryKey(Object id, Map<String, Object> params, String table, String database);
    
    Object insert(Map<String, Object> params, String table, String database);
    
    void clearExecuterCache(String bucket, String restName, String option, String version);
    
    void clearDALCache(String table, String database);
    
    QueryResult selectByCriteria(List<String> fields, Map<String, Object> params, String table, int seconds, String database, boolean isNoSql);
	
    QueryResult selectByPrimaryKey(List<String> fields, Object id, String table, int seconds, String database, boolean isNoSql);
    
    int countByCriteria(Map<String, Object> params, String table, int seconds, String database, boolean isNoSql);
    
    int deleteByPrimaryKey(Object id, String table, String database, boolean isNoSql);
    
    int deleteByCriteria(Map<String, Object> params, String table, String database, boolean isNoSql);
    
    int updateByPrimaryKey(Object id, Map<String, Object> params, String table, String database, boolean isNoSql);
    
    Object insert(Map<String, Object> params, String table, String database, boolean isNoSql);
    
	
	
    

}
